from django.conf.urls import url
from .views import subscribe, user_validate, profile, myajaxview
from django.views.generic.base import RedirectView

urlpatterns = [
	url('sub/', subscribe, name = 'subscribe'),
	url('see-profile/', profile, name = 'profile'),
    url('validate/', user_validate , name='validate_email'),
	url('data-view/', myajaxview , name='myajaxview')
]