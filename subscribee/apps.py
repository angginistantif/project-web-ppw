from django.apps import AppConfig


class SubscribeeConfig(AppConfig):
    name = 'subscribee'
