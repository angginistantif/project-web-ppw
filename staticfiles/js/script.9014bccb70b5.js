$(document).ready(function() {
   var accordionTitle = $('.accordion-title');
   var accordionText = $('.accordion-text');
   var speed = 300;

   accordionTitle.on('click', function() {
       var thisAccordion = $(this);
       var isActive = thisAccordion.hasClass('active');

       if( isActive ) {
           thisAccordion
               .removeClass('active')
               .parent().find('.accordion-text').slideUp(speed);
       } else {
           accordionTitle.removeClass('active');
           accordionText.slideUp(speed);
           thisAccordion
               .addClass('active')
               .parent().find('.accordion-text').slideDown(speed);
       }
   });

   if($(window).width() >= 1024){
    accordionText.slideUp(speed);
   }
});

$(document).ready(function(){
    $("#apayaakepo").click(function(){
        $("body").toggleClass("body2");
    });
});

$(document).ready(function(){
    $.ajax({
		url: "https://www.googleapis.com/books/v1/volumes?q=quilting",
		dataType: 'json',
		type: 'get',
		cache:false,
		success: function(data){
			var list_books = '';
			for (i in data.items){
				list_books += '<tbody><tr>';
                list_books += '<td>' + data.items[i].volumeInfo.title + '</td>'
                list_books += '<td>' + data.items[i].volumeInfo.authors + '</td>'
                list_books += '<td>' + data.items[i].volumeInfo.publisher + '</td>'
				list_books += '<td><img src=' + data.items[i].volumeInfo.imageLinks.thumbnail +'> </img></td>'
                list_books += '<td align ="center"><a href="#" id = "buttonLove" class="btn btn-danger" title="add fav"><i class="glyphicon glyphicon-thumbs-up" ></></a></td>'
                list_books += '</tr></tbody>';
            }
            $("#table_book").append(list_books);
        },
        error: function(d){
            console.log("error");
            alert("404! Please wait until the File is Loaded.");
        }
    });
});


