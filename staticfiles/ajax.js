$(document).ready(function () {
    var x_timer;
    $("#nama_form").keyup(function (e) {
        clearTimeout(x_timer);
        var password = $(this).val();
        x_timer = setTimeout(function () {validate_user();}, 10);

    });
});

$(document).ready(function () {
    var x_timer;
    $("#email_form").keyup(function (e) {
        clearTimeout(x_timer);
        var email = $(this).val();
        x_timer = setTimeout(function () {validate_user();}, 10);
    });
});

$(document).ready(function () {
    var x_timer;
    $("#password_form").keyup(function (e) {
        clearTimeout(x_timer);
        var nama = $(this).val();
        x_timer = setTimeout(function () {validate_user();}, 10);
    });
});

function validate_user() {
    $.ajax({
        url: "/subscribe/validate/",
        type: "POST",
        data: {
            nama: $('#nama_form').val(),
            email: $('#email_form').val(),
            password: $('#password_form').val()
        },

        success: function (response) {
            if (response.message == "All fields are valid"){
                document.getElementById('subscribe_button').disabled = false;
                $('#msg_validate').html("<p style='color:yellowgreen'>"+ response.message + "</p>")
            }
            else {
                document.getElementById('subscribe_button').disabled = true;
                $('#msg_validate').html("<p style='color:tomato'>"+ response.message + "</p>")
            }

        },
        error: function (errmsg) {
            console.log(errmsg + "ERROR DETECTED");
            alert("404! ERROR OMG");
        }

    });
};

function subscribe() {
    $.ajax({
        url: "/subscribe/sub/",
        type: "POST",
        data: {
            nama: $('#nama_form').val(),            
            email: $('#email_form').val(),
            password: $('#password_form').val()
        },
        success: function (json) {
            console.log(json);
            $('#subscribe_form').val(''); // empty form
            $('#msg_response').html("<div class='alert alert-success'><strong>Success!</strong> YAY thanks for subs meh!!</div>")
        },
        error: function (xhr, errmsg, err) {
            $('#msg_response').html("<div class='alert alert-danger'><strong>Error! </strong>OH no, there is some problem with the server:(</div>");
            alert("404! ERROR OMG");
        },
    });
};

