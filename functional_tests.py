import unittest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class TestStory6_FunctionalTest(unittest.TestCase):
	def setUp(self):
		chrome_options = Options()
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(TestStory6_FunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(TestStory6_FunctionalTest, self).tearDown()
	
	def test_input(self):
		selenium = self.selenium
		selenium.get('http://fairuzhanun.herokuapp.com/web_story6/status/')
		selenium.implicitly_wait(5)
		title = selenium.find_element_by_id('id_status_message')
		submit = selenium.find_element_by_id('submit')
		title.send_keys('Coba - Coba')
		submit.send_keys(Keys.RETURN)
		self.assertIn("Coba - Coba", self.selenium.page_source)
		
	def test_layout1(self):
		selenium = self.selenium
		selenium.get('http://fairuzhanun.herokuapp.com/web_story6/status/')
		selenium.implicitly_wait(5)
		submit = selenium.find_element_by_id('submit')
		self.assertIn(submit.text, self.selenium.page_source)
	
	def test_layout2(self):
		selenium = self.selenium
		selenium.get('http://fairuzhanun.herokuapp.com/web_story6/status/')
		selenium.implicitly_wait(5)
		self.assertIn("Lihat Profil ", self.selenium.page_source)
		
	def test_style(self):
		selenium = self.selenium
		selenium.get('http://fairuzhanun.herokuapp.com/web_story6/status/')
		selenium.implicitly_wait(5)
		content = selenium.find_element_by_css_selector('h1.greetings_layout')
		self.assertIn(content.text, self.selenium.page_source)
		
	def test_style2(self):
		selenium = self.selenium
		selenium.get('http://fairuzhanun.herokuapp.com/web_story6/status/')
		selenium.implicitly_wait(5)
		content = selenium.find_element_by_css_selector('h2.giving_layout')
		self.assertIn(content.text, self.selenium.page_source)
			
if __name__ == '__main__': #
    unittest.main(warnings='ignore') #
