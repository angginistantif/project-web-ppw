from django.apps import AppConfig


class WebStory6Config(AppConfig):
    name = 'web_story6'
