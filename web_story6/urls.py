from django.conf.urls import url
from django.views.generic.base import RedirectView
from .views import lihat_profil, status, lihat_books, data_buku

#url for app
app_name = "web_story6"
urlpatterns = [
	url(r'^$', RedirectView.as_view(url = 'status/')),
	url(r'status/', status, name = 'status'),
	url(r'profile/', lihat_profil, name = 'lihat_profil'),
	url(r'books/', lihat_books, name = 'lihat_books'),
	url(r'data/', data_buku, name = 'data_buku'),
]
