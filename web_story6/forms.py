from django import forms

from django.forms import Form
from .models import StatusModel


class StatusForm(forms.Form):
	created_date = forms.DateTimeInput(attrs={'class': 'form-control'})
	error_messages = {'required': 'This field is required.',}
	description_attrs = {
        'type': 'text',
        'cols': 75,
        'rows': 6,
        'class': 'todo-form-textarea',
        'placeholder':'Status...'
    }
	status_message = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))
	
	class Meta:
		model = StatusModel
		fields = ['status_message','created_date']