from django.urls import reverse
from .forms import StatusForm
from .models import StatusModel
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render


from django.shortcuts import render, render_to_response
from django.template import RequestContext, loader
import urllib.request, json, requests

# Create your views here.

response = {}
def lihat_profil(request):
	return render(request, 'profile.html')


def lihat_books(request):
	return render(request, 'books.html')

def data_buku(request):
	
	value = requests.get("https://www.googleapis.com/books/v1/volumes?q=computer");
	return HttpResponse(value, content_type='application/json')	

def status(request):
	todo = StatusModel.objects.all()
	if(request.method == 'POST'):
		form = StatusForm(request.POST or None)
		if(form.is_valid()):
			response['status_message'] = request.POST['status_message'];
			stat = StatusModel(status_message=response['status_message'])
			stat.save()
			return HttpResponseRedirect('/web_story6/status/')
	else:
		form = StatusForm()
	return render(request, 'simple_web_status.html',{'todo':todo, 'form':form})