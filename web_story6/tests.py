from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.core.files.uploadedfile import SimpleUploadedFile
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import status, lihat_profil
from .forms import StatusForm
from .models import StatusModel


# Create your tests here.
class TestStory6_UnitTest(TestCase):
	def test_url_is_exist(self):
		response = Client().get('/web_story6/profile/')
		self.assertEqual(response.status_code, 200)

	def test_using_landing_page_template(self):
		response = Client().get('/web_story6/status/')
		self.assertTemplateUsed(response, 'simple_web_status.html')

	def test_using_landing_page_template2(self):
		response = Client().get('/web_story6/profile/')
		self.assertTemplateUsed(response, 'profile.html')

	def test_using_index_func(self):
		found = resolve('/web_story6/status/')
		self.assertEqual(found.func, status)
		
	def test_models_can_create_new_status(self):
		#Creating a new activity
		new_status = StatusModel.objects.create(status_message='')

		#Retrieving all available activity
		counting_all_available_status = StatusModel.objects.all().count()
		self.assertEqual(counting_all_available_status, 1)

	def test_form_validation_for_blank_items(self):
		form = StatusForm(data={'status_message': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors['status_message'],
				["This field is required."]
		)

	def test_post_success_and_render_the_result(self):
		test = 'Hello'
		response_post = Client().post('/web_story6/status/', {'status_message': test})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/web_story6/status/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)	

	def test_lab5_post_error_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('/web_story6/status/', {'status_message': test })
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/web_story6/')
		html_response = response.content.decode('utf8')
		self.assertNotIn(test, html_response)

	def test_can_save_a_POST_request(self):
		response = self.client.post('/web_story6/status/', data = {'status_message' : 'testing'})
		counting_all_available_status = StatusModel.objects.all().count()
		self.assertEqual(counting_all_available_status, 1)

		self.assertEqual(response.status_code, 302)
		self.assertEqual(response['location'], '/web_story6/status/')

		new_response = self.client.get('/web_story6/')
		html_response = new_response.content.decode('utf8')
		self.assertIn('', html_response)


	def test_story6_content(self):
		request = HttpRequest()
		response = status(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Hello, Apa kabar?', html_response)
		self.assertIn('Masukan status:', html_response)

	def test_profil6_content(self):
		request = HttpRequest()
		response = lihat_profil(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Angginistanti Fairuz Hanun', html_response)
		self.assertIn('Anggis - 19 tahun', html_response)
		self.assertIn('Lahir di Tanjung Redeb, 25 September 1999', html_response)
		self.assertIn('Anak ke-2 dari 2 bersaudara', html_response)
		self.assertIn('Mahasiswi dari Program Studi Sistem Informasi, Fakultas Ilmu Komputer, Universitas Indonesia', html_response)
		self.assertIn('Memiliki hobi menulis, dan mendengar musik klasik', html_response)
		


 